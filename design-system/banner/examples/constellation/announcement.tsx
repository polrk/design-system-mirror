import React from 'react';
import Banner from '../../src';

export default function AnnouncementDemo() {
  return (
    <Banner appearance="announcement" isOpen>
      Announcement goes here
    </Banner>
  );
}
