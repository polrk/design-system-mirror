import React from 'react';
import Toggle from '../src';

export default () => (
  <div>
    <Toggle />
    <Toggle isDefaultChecked />
  </div>
);
