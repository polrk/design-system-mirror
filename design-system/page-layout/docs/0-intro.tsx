import React from 'react';
import { md, DevPreviewWarning } from '@atlaskit/docs';

export default md`
  ${(
    <div style={{ marginTop: '0.5rem' }}>
      <DevPreviewWarning />
    </div>
  )}
  `;
