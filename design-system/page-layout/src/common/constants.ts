// dimension vars
export const LEFT_PANEL_WIDTH = 'leftPanelWidth';
export const LEFT_SIDEBAR_WIDTH = 'leftSidebarWidth';
export const RIGHT_SIDEBAR_WIDTH = 'rightSidebarWidth';
export const RIGHT_PANEL_WIDTH = 'rightPanelWidth';
export const TOP_NAVIGATION_HEIGHT = 'topNavigationHeight';
export const BANNER_HEIGHT = 'bannerHeight';
export const LEFT_SIDEBAR_FLYOUT = 'leftSidebarFlyoutWidth';
export const DIMENSIONS = [
  LEFT_PANEL_WIDTH,
  RIGHT_PANEL_WIDTH,
  BANNER_HEIGHT,
  TOP_NAVIGATION_HEIGHT,
  LEFT_SIDEBAR_WIDTH,
  RIGHT_SIDEBAR_WIDTH,
];

// Grid area names
export const LEFT_PANEL = 'left-panel';
export const RIGHT_PANEL = 'right-panel';
export const BANNER = 'banner';
export const TOP_NAVIGATION = 'top-navigation';
export const CONTENT = 'content';
export const MAIN = 'main';
export const LEFT_SIDEBAR = 'left-sidebar';
export const RIGHT_SIDEBAR = 'right-sidebar';

// Other constants
export const COLLAPSED_LEFT_SIDEBAR_WIDTH = 20;
export const MIN_LEFT_SIDEBAR_WIDTH = 80;
export const LEFT_SIDEBAR_FLYOUT_WIDTH = 240;
export const MIN_LEFT_SIDEBAR_DRAG_THRESHOLD = 200;
export const DEFAULT_SIDEBAR_WIDTH = 300;
export const LEFT_SIDEBAR_EXPANDED_WIDTH = 'expandedLeftSidebarWidth';
export const PAGE_LAYOUT_LS_KEY = 'PAGE_LAYOUT_UI_STATE';

// Data attributes
export const IS_SIDEBAR_DRAGGING = 'data-is-sidebar-dragging';
export const IS_SIDEBAR_COLLAPSING = 'data-is-sidebar-collapsing';
export const IS_SIDEBAR_COLLAPSED = 'data-is-sidebar-collapsed';
export const IS_FLYOUT_OPEN = 'data-is-flyout-open';
export const GRAB_AREA_LINE_SELECTOR = 'data-grab-area-line';
export const RESIZE_BUTTON_SELECTOR = 'data-resize-button';
export const RESIZE_CONTROL_SELECTOR = 'data-resize-control';
export const MAIN_SELECTOR = 'data-main';
export const LEFT_SIDEBAR_SELECTOR = 'data-left-sidebar';
