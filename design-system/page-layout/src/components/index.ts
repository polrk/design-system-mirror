export {
  PageLayout,
  Main,
  RightPanel,
  LeftPanel,
  RightSidebar,
  LeftSidebar,
  Banner,
  TopNavigation,
  Content,
} from './slots';

export { default as ResizeControl } from './resize-control';
