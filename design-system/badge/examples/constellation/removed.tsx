import React from 'react';
import Badge from '../../src';

const Removed = () => <Badge appearance="removed">-100</Badge>;

export default Removed;
