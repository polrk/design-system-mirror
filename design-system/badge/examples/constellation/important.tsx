import React from 'react';
import Badge from '../../src';

const Important = () => <Badge appearance="important">{25}</Badge>;

export default Important;
