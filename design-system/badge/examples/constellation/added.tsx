import React from 'react';
import Badge from '../../src';

const Added = () => <Badge appearance="added">+100</Badge>;

export default Added;
