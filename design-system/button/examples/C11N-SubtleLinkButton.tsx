import React from 'react';
import Button from '../src';

export default () => (
  <Button appearance="subtle-link">Subtle Link Button</Button>
);
