import React from 'react';
import Avatar from '../src';

const AvatarHover = () => <Avatar name="Avatar name" size="medium" />;

export default AvatarHover;
