import React from 'react';
import Avatar from '../src';

const AvatarSquare = () => <Avatar appearance="square" />;

export default AvatarSquare;
