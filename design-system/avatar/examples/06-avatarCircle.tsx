import React from 'react';
import Avatar from '../src';

const AvatarCircle = () => <Avatar appearance="circle" />;

export default AvatarCircle;
