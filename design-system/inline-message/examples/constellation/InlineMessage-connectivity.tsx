import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageConnectivity() {
  return (
    <InlineMessage type="connectivity">
      <p>Connectivity type dialog</p>
    </InlineMessage>
  );
}
