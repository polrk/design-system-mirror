import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageConfirmation() {
  return (
    <InlineMessage title="Title">
      <p>Dialog</p>
    </InlineMessage>
  );
}
