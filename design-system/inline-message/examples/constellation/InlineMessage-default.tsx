import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageDefault() {
  return (
    <InlineMessage>
      <p>Default type dialog</p>
    </InlineMessage>
  );
}
