import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageInfo() {
  return (
    <InlineMessage type="info">
      <p>Info type dialog</p>
    </InlineMessage>
  );
}
