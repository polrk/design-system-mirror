import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageError() {
  return (
    <InlineMessage type="error">
      <p>Error type dialog</p>
    </InlineMessage>
  );
}
