import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageWarning() {
  return (
    <InlineMessage type="warning">
      <p>Warning type dialog</p>
    </InlineMessage>
  );
}
