import React from 'react';
import InlineMessage from '../../src';

export default function InlineMessageConfirmation() {
  return (
    <InlineMessage type="confirmation">
      <p>Confirmation type dialog</p>
    </InlineMessage>
  );
}
