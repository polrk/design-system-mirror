import React from 'react';
import { AtlassianLogo } from '../../src';

export default () => <AtlassianLogo size="large" />;
