import React from 'react';
import { Section, ButtonItem } from '../src';

export default () => (
  <Section title="Actions">
    <ButtonItem>Create article</ButtonItem>
  </Section>
);
