import React from 'react';
import SectionMessage from '../../src';

const ChangeSectionMessage = () => (
  <SectionMessage title="Important information" appearance="change">
    <p>
      For communicating a change, important information or an update to a
      significant part of the UI.
    </p>
  </SectionMessage>
);

export default ChangeSectionMessage;
