import React from 'react';
import SectionMessage from '../../src';

const ConfirmationSectionMessage = () => (
  <SectionMessage title="Success" appearance="confirmation">
    <p>Communicates that an action has been successful.</p>
  </SectionMessage>
);

export default ConfirmationSectionMessage;
