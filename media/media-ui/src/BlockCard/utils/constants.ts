export const containerCentredStyles = {
  display: 'flex',
  justifyContent: 'space-between',
  alignItems: 'center',
};

const CDN_BASE_URL =
  'https://smart-links-cdn.us-east-1.prod.public.atl-paas.net';
export const LockImage = `${CDN_BASE_URL}/lock.svg`;
export const CelebrationImage = `${CDN_BASE_URL}/celebration.svg`;
