import FieldsLoader from './FieldsLoader';

export { isNativeFieldType } from './utils';

export default FieldsLoader;
